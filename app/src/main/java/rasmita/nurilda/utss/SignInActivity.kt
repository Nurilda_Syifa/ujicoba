package rasmita.nurilda.utss

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(), View.OnClickListener{
    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogoff.setOnClickListener {
            fbAuth.signOut()
        }
        btnbb.setOnClickListener(this)
        btno.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnbb->{
                val intent = Intent(this,BhnActivity::class.java)
                startActivity(intent)
            }
            R.id.btno->{
                val intent = Intent(this,OutletActivity::class.java)
                startActivity(intent)
            }
        }
    }

}